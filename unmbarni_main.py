#!/usr/bin/env python3
# unmbarni_main.py
#
# last modified: ajm, 28DEC2020
#

# Import modules
# Python Standard Library:
import sys
import os
import textwrap as tw
# Local:
import process_settings
from preprocess import process_library, process_data, process_grdtruth, \
process_extresults
from analyze import analyze_spectrum, calc_results, match_nuclides
from postprocess import plot_spectrum

# Version number
VER = 1.2

# Perform version and library checks; import third-party libraries.
py_maj_ver = int(sys.version[0])
if py_maj_ver < 3:
    sys.exit("Fatal error: Not compatible with Python 2. Aborting...")
try:
    import numpy
except ImportError:
    sys.exit("Fatal Error: Missing required NumPy library. Aborting...")

# Read in user-supplied input file, which specifies all the other necessary
# files to run the job. Also, read user-supplied string to use for naming
# the output text and plot files. If no output string is provided, provide a
# default string.
if len(sys.argv) < 2:
    sys.exit("Fatal Error: Must at least specify an input file. Aborting...")
else:
    input_file = str(sys.argv[1])

if len(sys.argv) < 3:
    output_name="unmbarni_output"
else:
    output_name = str(sys.argv[2])
output_file = "{}.txt".format(output_name)
output = open(output_file, 'w')

if "unmbarni_defaults.xml" in os.listdir():
    def_file = "unmbarni_defaults.xml"
else:
    sys.exit("Fatal Error: barni_defaults.xml is missing. Aborting...")

# Introduce program
p1 = """
=============================================================================

  uu  uu  nnnnnn  mmmmmmmmmm
  uu  uu  nn  nn  mm  mm  mm
  uuuuuu  nn  nn  mm  mm  mm

  %%%%%%%%%%%%%    %%%%%%%%%%%%%%   %%%%%%%%%%%%      %%%%%       %%%   %%%
  %%%        %%%   %%%        %%%   %%%       %%%     %%%%%%      %%%   %%%
  %%%         %%%  %%%        %%%   %%%        %%%    %%% %%%     %%%   %%%
  %%%        %%%   %%%        %%%   %%%       %%%     %%%  %%%    %%%   %%%
  %%%%%%%%%%%%%    %%%%%%%%%%%%%%   %%%%%%%%%%%%      %%%   %%%   %%%   %%%
  %%%        %%%   %%%        %%%   %%%       %%%     %%%    %%%  %%%   %%%
  %%%         %%%  %%%        %%%   %%%        %%%    %%%     %%% %%%   %%%
  %%%        %%%   %%%        %%%   %%%         %%%   %%%      %%%%%%   %%%
  %%%%%%%%%%%%%    %%%        %%%   %%%          %%%  %%%       %%%%%   %%%
=============================================================================

unmBARNI: Benchmarking Algorithm for RadioNuclide Identification
(University of New Mexico Version)

Version {}
Created by: Adam J Morton, 2018-2021
Code Repository: https://gitlab.com/ajmorton90/unmbarni
Please see the README and LICENSE files for additional information.
*************************************************************************

**** START BARNI JOB ****""".format(VER)

p2 = """
Using input file: {}

Performing analysis with SciPy signal tools.""".format(input_file)

# Introduce program
print(p1)
print(p2)
output.write(p1)
output.write("""
{}""".format(p2))

# Retrieve the dictionary of runtime parameters, including defaults
barni_defs = process_settings.get_defaults(def_file)
run_params = process_settings.parse_settings(input_file, barni_defs)

# Read in entries from library file
lib_file = run_params["Library"]
lib = process_library.get_library(lib_file)

# Read in spectrum file...
spec_file = run_params["Spectrum"]
energy_bins, bin_counts = process_data.get_spectrum(spec_file)
# ...and convert data to ndarrays
energy_array = np.array(energy_bins, dtype=float)
counts_array = np.array(bin_counts, dtype=float)

# Read in ground truth file
ans_file = run_params["GroundTruth"]
answer_nuclides = process_grdtruth.get_answers(ans_file)

# Set remaining runtime parameters from the settings dictionary
N_OTHERS = int(run_params["NumOtherResults"])
WIDTH = run_params["SGWindowWidth"]
ORDER = run_params["SGOrder"]
TOL = run_params["Tolerance"]
DIST = run_params["Distance"]
PROM = run_params["PeakProminence"]
P_LO = run_params["PlotLow"]
P_HI = run_params["PlotHigh"]
SHOW_SMOOTH = run_params["ShowSmooth"]

# Read in data from other results, if applicable
if N_OTHERS > 0:
    others_file = process_settings.get_others_file(input_file)
    if others_file == "ERR":
        # TODO: Print this error to the output file as well.
        print(tw.dedent("""
        Warning: Could not find filename for other results. Proceeding with \
        BARNI analysis only."""))
        N_OTHERS = 0
    else:
        others_dict = process_extresults.get_others(others_file)

# Perform the internal analysis using SciPy signal tools
smoothed, peak_idx = analyze_spectrum.find_peaks_savgol(counts_array, WIDTH,
                                                        ORDER, PROM, DIST)

# Use the peaks found to match radionuclides
nm_dict = match_nuclides.id_nuclides(counts_array, energy_array, peak_idx, lib,
                                    TOL)

# Calculate the confidence metric for each of the results
cm_dict = match_nuclides.get_confidence(nm_dict)

# Get nuclide names from the answer key
ans_names = []
for a in answer_nuclides:
    ans_names.append(a[0])
ans_names = set(ans_names)

# Calculate the F-score and its necessary parameters
found_names = []
for key, value in nm_dict.items():
    found_names.append(key[0])
found_names = set(found_names)

fscore_params = calc_results.calc_parameters(found_names, ans_names)
b_p = calc_results.calc_precision(fscore_params['true_pos'],
                                  fscore_params['false_pos'])
b_r = calc_results.calc_recall(fscore_params['true_pos'],
                               fscore_params['false_neg'])
b_f = calc_results.calc_fscore(b_p, b_r)

# If comparing with results from other analysis source(s), calculate the
# F-scores for those as well
if N_OTHERS > 0:
    others_scores = {}
    for key, value in others_dict.items():
        value = set(value)
        oth_fscore_params = calc_results.calc_parameters(value, ans_names)
        oth_p = calc_results.calc_precision(oth_fscore_params['true_pos'],
                                            oth_fscore_params['false_pos'])
        oth_r = calc_results.calc_recall(oth_fscore_params['true_pos'],
                                         oth_fscore_params['false_neg'])
        oth_f = calc_results.calc_fscore(oth_p, oth_r)
        ind = oth_f / b_f
        others_scores[key] = (oth_p, oth_r, oth_f, ind)

# Print the results
p3 = """
=========================================================================
                                 REPORT
=========================================================================

BARNI identified {} nuclide(s) in the spectrum under test. They are:
--------------------------------------
|    Name | Category    | Confidence |
--------------------------------------""".format(len(found_names))
print(p3)
output.write("""
{}""".format(p3))

# Sort the identification results by confidence metric
nuc_info = []
for key, value in cm_dict.items():
    nuc_info.append((key, value))
nuc_info = sorted(nuc_info, key=lambda x: x[1], reverse=True)

for n in nuc_info:
    name = n[0][0]
    category= n[0][1]
    conf = n[1] * 100
    f = '| {0:>7} | {1:10}  | {2:<5}      |'
    p4 = f.format(name, category, round(conf, 2))
    print(p4)
    output.write(tw.dedent("""
    {}""".format(p4)))

p5a = """--------------------------------------

The user-supplied answer key specified {} nuclide(s) in the spectrum
under test. They are:
-------------------------
|    Name | Category    |
-------------------------""".format(len(answer_nuclides))
print(p5a)
output.write("""
{}""".format(p5a))

for a in answer_nuclides:
    f = '| {0:>7} | {1:10}  |'
    name = a[0]
    category= a[1]
    p5b = f.format(name, category)
    print(p5b)
    output.write(tw.dedent("""
    {}""".format(p5b)))

p5c = """-------------------------

The resulting benchmark performance evaluation is:
--------------------------------------
Calculated precision = {0:<5}         |
Calculated recall    = {1:<5}         |
Resulting F-score    = {2:<5}         |
--------------------------------------""".format(round(b_p, 3), round(b_r, 3),
                                                 round(b_f, 3))
print(p5c)
output.write("""
{}""".format(p5c))

# Include results from other analysis source(s), if applicable
if N_OTHERS > 0:
    p6a = tw.dedent("""
    This benchmark was compared with the scores from {} external analysis
    sources.""".format(N_OTHERS))
    p6b = tw.dedent("""
    The results of the comparison are:
    ---------------------------------------------------------------
    |     Source     |  Precision  |  Recall  |  F-Score  |  BI*  |
    ---------------------------------------------------------------""")
    print(p6a)
    print(p6b)
    output.write(tw.dedent("""
    {}""".format(p6a)))
    output.write(tw.dedent("""
    {}""".format(p6b)))

    f1 = "|  {0:<12}  |    {1:<5}    |   {2:<5}  |   {3:<5}   | {4:<5} |"
    b_key, b_ind = "BARNI", "--"
    p7 = f1.format(b_key, round(b_p, 3), round(b_r, 3), round(b_f, 3),
          b_ind)
    p8 = """---------------------------------------------------------------"""
    print(p7)
    print(p8)
    output.write(tw.dedent("""
    {}""".format(p7)))
    output.write(tw.dedent("""
    {}""".format(p8)))

    for key, value in others_scores.items():
        oth_p, oth_r, oth_f, ind = value
        p9 = f1.format(key, round(oth_p, 3), round(oth_r, 3), round(oth_f, 3),
              round(ind, 3))
        print(p9)
        print(p8)
        output.write(tw.dedent("""
    {}""".format(p9)))
        output.write(tw.dedent("""
    {}""".format(p8)))
    p11 = tw.dedent("""
    *BI = BARNI Index; the F-score of a given external algorithm, normalized to
    the BARNI baseline F-score (i.e., BI is a multiple of the baseline score.
    """)
    print(p11)
    output.write(tw.dedent("""
    {}""".format(p11)))

else:
    p6 = tw.dedent("""
    No external sources of data analysis were provided for comparison to the
    benchmarking algorithm.""")
    print(p6)
    output.write(tw.dedent("""
    {}""".format(p6)))

plot_file = "{}.png".format(output_name)
p12 = """
Output written to: {}
Plot saved as: {}

**** END BARNI JOB ****
""".format(output_file, plot_file)
print(p12)
output.write("""
{}""".format(p12))
output.close()

# Plot the results
if SHOW_SMOOTH:
    plot_spectrum.plot_results(energy_array, counts_array,
                               nm_dict, (P_LO, P_HI), smoothed)
else:
    plot_spectrum.plot_results(energy_array, counts_array,
                               nm_dict, (P_LO, P_HI))