#!/usr/bin/env python3
# process_settings.py
#
# last modified: ajm, 28DEC2020
#

# Import modules
# Python Standard Library:
import xml.etree.ElementTree as ET

def get_defaults(defaults_file):
    tree = ET.parse(defaults_file)
    root = tree.getroot()
    defaults_dict = {}
    for child in root:
        defaults_dict[child.tag] = int(child.text)
    return defaults_dict

def get_others_file(barni_input):
    tree = ET.parse(barni_input)
    root = tree.getroot()
    try:
        entry = root.find("OtherResults")
        entry_val = entry.text
    except AttributeError:
        entry_val = "ERR"
    return entry_val

def parse_settings(barni_input, barni_defaults):
    tree = ET.parse(barni_input)
    root = tree.getroot()

    required_inp = ["Library", "Spectrum", "GroundTruth", "NumOtherResults"]
    optional_inp = ["SGWindowWidth", "SGOrder", "PeakProminence", "Tolerance",
                    "Distance", "PlotLow", "PlotHigh", "ShowSmooth"]
    run_options = {}
    for setting in required_inp:
        try:
            entry = root.find(setting)
            entry_val = entry.text
        except AttributeError:
            entry_val = "ERR"
        run_options[setting] = entry_val

    for setting in optional_inp:
        try:
            entry = root.find(setting)
            entry_val = int(entry.text)
        except AttributeError:
            entry_val = barni_defaults[setting]
        run_options[setting] = entry_val
    return run_options