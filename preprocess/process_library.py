#!/usr/bin/env python3
# process_library.py
#
# last modified: ajm, 28DEC2020
#

# Import modules
# Python Standard Library:
import csv

def decomment(file):
    """ Removes comments (designated by "#") from input CSV files."""
    for row in file:
        raw = row.split("#")[0].strip()
        if raw: yield raw

def get_library(filename):
    """ Parses a reference nuclide library to be used for peak identification.  

    Parameters:
    filename (string) -- path to CSV file from which to parse the library.
                         The file path is read from the user input file by
                         barni_main.py and passed to this function.

    Returns:
    library (tuple) -- collection of dictionary (with category and gamma
                       energies corresponding to each nuclide in the library)
                       and consolidated set of energies across all nuclides
                       (zeros and duplicates are removed).
    """
    with open(filename, 'r') as reflib:
        csv_reader = csv.reader(decomment(reflib))

        nucdict = {}
        enlist = []

        for row in csv_reader:
            nuc = row[0]
            cat = row[1]

            en1 = int(row[2])
            enlist.append(en1)

            en2 = int(row[3])
            enlist.append(en2)

            en3 = int(row[4])
            enlist.append(en3)

            if (en2 != 0) and (en3 != 0):
                nucdict[nuc,cat] = (en1,en2,en3)
            elif (en2 != 0) and (en3 == 0):
                nucdict[nuc,cat] = (en1, en2)
            else:
                nucdict[nuc,cat] = (en1,)

    enlist = set(enlist)
    for E in enlist:
        if E == 0:
            del(E)

    library = (nucdict, enlist)
    return library