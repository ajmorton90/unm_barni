#!/usr/bin/env python3
# process_extresults.py
#
# last modified: ajm, 28DEC2020
#

# Import modules
# Python Standard Library:
import csv

def decomment(file):
    """ Removes comments (designated by "#") from input CSV files."""
    for row in file:
        raw = row.split("#")[0].strip()
        if raw: yield raw

def get_others(filename):
    """ Parses results from the other algorithm sources to be evaluated.

    Parameters:
    filename (string) -- path to CSV file from which to parse results of other
                         algorithms. The file path is read from the user input
                         file by barni_main.py and passed to this function.

    Returns:
    other_results (dictionary) -- collection of algorithm names (keys) and
                                  corresponding lists of nuclides (values).
    """
    with open(filename, 'r') as others:
        csv_reader = csv.reader(decomment(others))

        other_results = {}

        for row in csv_reader:
            alg = row[0]
            other_results[alg] = row[1].split(sep='&')
    return(other_results)