#!/usr/bin/env python3
# process_data.py
#
# last modified: ajm, 28DEC2020
#

# Import modules
# Python Standard Library:
import csv

def get_spectrum(filename):
    """ Parses data from the spectrum under test for analysis by BARNI.   

    Parameters:
    filename (string) -- path to CSV file from which to parse the data.
                         The file path is read from the user input file by
                         barni_main.py and passed to this function.

    Returns:
    spectrum (tuple) -- collection of a list of energy bins and a list of counts
                        corresponding to each bin.
    """
    with open(filename, "r") as spec_data:
        csv_reader = csv.reader(spec_data)

        energies = []
        counts = []

        for row in csv_reader:
            E = row[0]
            N = row[1]

            energies.append(E)
            counts.append(N)

    spectrum = (energies, counts)
    return(spectrum)