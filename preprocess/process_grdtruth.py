#!/usr/bin/env python3
# process_grdtruth.py
#
# Created by Adam J Morton on 06 December 2018
# Last modified: ajm, 28DEC2020
#

# Import modules
# Python Standard Library:
import csv

def decomment(file):
    """ Removes comments (designated by "#") from input CSV files."""
    for row in file:
        raw = row.split("#")[0].strip()
        if raw: yield raw

def get_answers(filename):
    """ Parses the answer key corresponding to the spectrum under test.

    Parameters:
    filename (string) -- path to CSV file from which to parse the answer key.
                         The file path is read from the user input file by
                         barni_main.py and passed to this function.

    Returns:
    answerkey (set) -- set of tuples that represent the correct answers for
                       the spectrum under test. Each tuple contains the
                       nuclide name and the corresponding category.
    """
    with open(filename, 'r') as answers:
        csv_reader = csv.reader(decomment(answers))

        answerkey = []

        for row in csv_reader:
            nuc = row[0]
            cat = row[1]
            answerkey.append((nuc, cat))

    answerkey = set(answerkey)
    return(answerkey)