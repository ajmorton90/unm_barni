#!/usr/bin/env python3
# spectrum_analysis.py
#
# last modified: ajm, 28DEC2020
#

# Import modules
# Python Standard Library:
import sys
# Third-party:
try:
    from scipy.signal import find_peaks
except ImportError:
    sys.exit("Fatal Error: Missing required SciPy library. Aborting...")
# The remaining imports should already have been verified at this point...
from scipy.signal import savgol_filter
import numpy as np

# Function for smoothing spectrum and extracting peaks
def find_peaks_savgol(counts, width, order, prom, dist):
    smooth_spec = savgol_filter(counts, width, order)
    found_peaks = find_peaks(smooth_spec, prominence=prom, distance=dist)
    peak_indices = np.array(found_peaks[0])
    return(smooth_spec, peak_indices)