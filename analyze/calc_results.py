#!/usr/bin/env python3
# calc_results.py
#
# last modified: ajm, 28DEC2020
#

def calc_parameters(found, answers):
    """ Calculates true positives, false positives, and false negatives.

    Parameters:
    found (set)   -- collection of nuclides identified by an algorithm.
    answers (set) -- collection of correct nuclides (from the answer key) for
                     the spectrum under test.

    Returns:
    params (dictionary) -- calculated values that correspond to the parameters
                           (keys) of true positives (tp), false positives (fp),
                           and false negatives (fn), all of which are used for
                           calculating precision and recall.
    """

    # Perform comparison with answer key
    # First, score true positives...
    tp_set = found & answers
    tp = len(tp_set)

    # False positives...
    fp_set = []
    for n in found:
        if n not in answers:
            fp_set.append(n)
    fp = len(fp_set)

    # And false negatives...
    fn_set = []
    for n in answers:
        if n not in found:
            fn_set.append(n)
    fn = len(fn_set)

    # Return a dictionary of the three (3) calculated values.
    params = {'true_pos': tp, 'false_pos': fp, 'false_neg': fn}
    return params

def calc_precision(tp, fp):
    """ Calculates the ratio of true positives to all reported nuclides.

    Parameters:
    tp (integer) -- number of true positives.
    fp (integer) -- number of false positives.

    Returns:
    precision (float) -- ratio of true positives to number of reported
                         nuclides.
    """
    if (tp + fp) != 0:
        precision = tp / (tp + fp)
    else:
        precision = 0
    return precision

def calc_recall(tp, fn):
    """ Calculates the ratio of true positives to correct (reference) nuclides.

    Parameters:
    tp (integer) -- number of true positives.
    fn (integer) -- number of false negatives.

    Returns:
    recall (float) -- ratio of true positives to actual nuclides present.
    """
    if (tp + fn) != 0:
        recall = tp / (tp + fn)
    else:
        recall = 0
    return recall

def calc_fscore(precision, recall):
    """ Calculates the F-score.

    Parameters:
    precision (float) -- ratio of true positives to number of reported
                         nuclides.
    recall (float)    -- ratio of true positives to number of actual nuclides
                         present.

    Returns:
    fscore (float) -- first harmonic mean of precision and recall, for a given
                      algorithm and with respect to the spectrum under test.
    """
    if (precision + recall) != 0:
        fscore = 2. * (precision * recall) / (precision + recall)
    else:
        fscore = 0.
    return fscore