#!/usr/bin/env python3
# match_nuclides.py
#
# last modified: ajm, 28DEC2020
#

# Import modules
# Third-party:
import numpy as np

def id_nuclides(C_data, E_data, peaks, library, tolerance):
    # Unpack the library
    lib_nuclides, lib_energies = library

    # Use peak indices to make a list of corresponding energies
    energy_list = []
    counts_list = []
    for i in peaks:
        E = E_data[i]
        energy_list.append(E)
        C = C_data[i]
        counts_list.append(C)
    # Iterate over found peak energies and test to see if any match the
    # library list, within the tolerance specified
    peaks_matched = []
    for i, e in enumerate(energy_list):
        e = float(e)
        for l in lib_energies:
            if abs(e - l) <= tolerance:
                c = counts_list[i]
                peaks_matched.append((l, e, c))
    results_dict = {}
    for key, value in lib_nuclides.items():
        num = len(value)
        e_count = 0
        p_list = []
        for v in value:
            for p in peaks_matched:
                if v != 0:
                    if p[0] == v:
                        p_list.append(p)
                        e_count = e_count + 1
        if num == 3:
            if e_count >= 2:
                results_dict[key] = (p_list, num)
        elif num < 3:
            if e_count >= 1:
                results_dict[key] = (p_list, num)
    return results_dict

def get_confidence(results_dict):
    conf_dict = {}
    for key, value in results_dict.items():
        num = len(value[0])
        nuc_conf = 0
        for p in range(0, num):
            v = value[0][p]
            E_lib = v[0]
            delta_E = abs(v[1] - E_lib)
            f_E = 1 - (delta_E/E_lib)
            C = v[2]
            rel_sigma = np.sqrt(C) / C
            f_s = 1 - rel_sigma
            conf = f_E * f_s
            nuc_conf += conf
        lib_num = value[1]
        f_p = num / (lib_num**2.)
        total_conf = nuc_conf * f_p
        conf_dict[key] = total_conf
    return conf_dict