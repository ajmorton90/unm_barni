#!/usr/bin/env python3
# plot_spectrum.py
#
# last modified: ajm, 27DEC2020
#

# Import modules
# Third-party:
try:
    import matplotlib.pyplot as plt
except ImportError:
    sys.exit("Fatal Error: Missing required Matplotlib library. Aborting...")


def plot_results(energies, counts, results, x_lims, smoothed_data=None)
"""Plot the spectrum with identified peaks."""

    plt.figure(figsize=(10, 6))
    plt.plot(energies, counts)
    ymax = max(counts)
    txt_interval = ymax / 10.
    txt_interval_2 = txt_interval / 4.
    xmin, xmax = x_lims

    if smoothed_data:
        plt.plot(energies, smoothed_data, 'k--', label="Smoothed Spectrum Overlay")
        plt.legend()

    V1 = 0.0
    for key, value in results.items():
        n_label = key[0]
        V1 += txt_interval
        V2 = 0.0
        for line in sorted(value[0]):
            E = line[0]
            V2 += txt_interval_2
            label_hgt = ymax - V1 - V2
            plt.vlines(x=E, ymin=0, ymax=ymax, linestyles='--',
                       linewidths=1.0, colors='r')
            plt.text((E+0.1), label_hgt, n_label, fontstyle='italic')

    plt.xlim(xmin, xmax)
    plt.title("BARNI Peak Identification")
    plt.xlabel("Energy [keV]")
    plt.ylabel("Gross Counts (per energy bin)")
    plt.savefig(plot_file)

    # Uncomment next line for on-the-fly plotting:
    #plt.show()