# unmBARNI: Benchmarking Algorithm for RadioNuclide Identification

## (University of New Mexico Version)

Current Version: 1.2

 - [Overview](https://gitlab.com/ajmorton90/unm_barni#overview)
 - [The Code](https://gitlab.com/ajmorton90/unm_barni#the-code)
 - [Usage](https://gitlab.com/ajmorton90/unm_barni#usage)
 - [Future Work](https://gitlab.com/ajmorton90/unm_barni#future-work)
 - [License and POC](https://gitlab.com/ajmorton90/unm_barni#license-and-poc)

## Overview

unmBARNI is an open-source tool for assessing the performance of gamma radionuclide
identification algorithms.
It is comprised of two parts:

1. a "simple" baseline algorithm that uses peak finding and table
searches to identify radionuclides; and

2. a statistics module to compare the performance of other algorithms with that
of the baseline algorithm, given the same test data.

In the context of this project, the "performance" of an algorithm is defined in
terms of F-scores.
Using this metric with a common baseline allows for fair
performance evaluations.
Typically, my colleagues in industry like to provide lists of what their latest
and greatest products "can" (<i>may</i>) do, with no reference point, and little
to no mention of what they <b>cannot</b> do.
Moreover, while some tools do exist to assist with fair performance evaluations, they are
not always easily accessible and may be subject to restricted distribution.

<b>The goal for unmBARNI is to provide a means to determine the strengths and weaknesses
of various detectors and algorithms in an unbiased fashion, and to make this means
open source and accessible to those involved with research and development or test and
evaluation of radionuclide identification systems.</b>

<i>NOTE:</i> unmBARNI is separate from the BARNI project being developed at
Lawrence Livermore National Laboratory (LLNL), which extends the concept of unmBARNI into
the world of machine learning methods.
The former, non-LLNL BARNI v1.1 (which was hosted on GitHub) was re-branded as unmBARNI v1.0.

## The Code

This project is written in [Python](https://www.python.org) (Version 3) and
makes use of the [NumPy](https://numpy.org),
[SciPy](https://www.scipy.org), and [Matplotlib](https://matplotlib.org)
libraries.
These libraries, which are nearly ubiquitous in the Python world and freely
available, are the only dependencies required beyond the Python Standard
Library.
You can use ```pip```, ```conda```, build from source, or (whatever
other method of choice) to install the external libraries. unmBARNI does not care
how you do it, but you need to do it.

## Usage

Provided that Python and the aforementioned libraries are installed, and that
all unmBARNI Python files are in the same working directory, the code can be run
from the command line:

```
$ python barni_main.py [/path/to/input_file.xml] [output_string]
```

The input XML file is required. See the barni\_input.xml file in this
repository for guidance in how to properly fill it out.
The output\_string is optional.
If provided, the name of the output text file
will be "\[output\_string\].txt", and the plot image will be named
"\[output\_string\].png".
If not provided, output\_string will default to "barni_output".
Regardless of the names, both output files will be created in the same
directory as the unmBARNI Python files.

Additionally, the "examples" folder in this repository contains guidance on how
to fill out the various mandatory and optional CSV files that are referenced by
the input XML file.
The input and CSV files need not be in the same directory as the rest of unmBARNI,
since file paths are specified on the command line and in the input file itself,
respectively.

## Future Work

Documentation is in the works; more information (including the User Manual) is
forthcoming.
(NB: Since there is currently no User Manual, I am obliged <i>not</i> to respond
with a [RTFM](https://en.wikipedia.org/wiki/RTFM) nastygram if you ask for help,
so please do not hesitate to contact me with questions.
I will be more than happy to assist you. My contact information is below.)

The unmBARNI Project is being developed as partial fulfillment of graduate study
(Master of Science in Nuclear Engineering) at the University of New Mexico
(UNM), located in the sunny high desert of Albuquerque, NM.

At this time, the code is nearly complete for the purposes that I require.
However, after I finish my thesis, I may continue to work on adding
features, improving the user interface and documentation, and overall making
the code more Pythonic.

## License and POC

This software is free (as in both beer and freedom) and open-source; it is
licensed under the MIT License. Please see the LICENSE file for the exact terms
of use.

The point of contact for the unmBARNI Project is Adam J Morton, currently
reachable via: ajmorton \[AT\] unm.edu

This README was last updated by AJM on 28DEC2020.
